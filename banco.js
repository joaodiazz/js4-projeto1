let Banco = {
  conta: "1234",
  saldo: 1000,
  tipo: "poupança",
  agencia: "Nu",
};

let saldo = function () {
  console.log(`Saldo atual: ${Banco.saldo}`);
};

let depositar = function (valor) {
  return (Banco.saldo = Banco.saldo + valor);
};

let sacar = function (valor) {
  return (Banco.saldo = Banco.saldo - valor);
};

let conta = function () {
  console.log(`Número da conta: ${Banco.conta}`);
};

saldo();
depositar(100);
saldo();
sacar(200);
saldo();
conta();
